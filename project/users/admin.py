from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin

from .models import User


@admin.register(User)
class UserAdmin(DjangoUserAdmin):

    list_display = ('id', ) + DjangoUserAdmin.list_display

    readonly_fields = ('id', )

    fieldsets = ((None, {'fields': ('id',)}),) + DjangoUserAdmin.fieldsets

    search_fields = ('id', ) + DjangoUserAdmin.search_fields
