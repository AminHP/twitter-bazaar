from rest_framework import viewsets

from drf_psq import PsqMixin, Rule

from .models import User
from .serializers import UserBasicSerializer, UserFullSerializer
from ..commons.permissions import (
    AllowAny, IsAuthenticated, IsAdminUser, AllowOptions,
)
from .permissions import IsSelf


class UserViewSet(PsqMixin, viewsets.ModelViewSet):

    queryset = User.objects.all()
    serializer_class = UserFullSerializer
    permission_classes = [AllowAny | AllowOptions]

    psq_rules = {
        'list': [Rule([IsAdminUser], UserFullSerializer)],

        'create': [Rule([AllowAny], UserBasicSerializer)],

        ('retrieve', 'update', 'partial_update'): [
            Rule([IsAdminUser], UserFullSerializer),
            Rule([IsAuthenticated & IsSelf], UserBasicSerializer),
        ],

        'destroy': [Rule([IsAdminUser])],
    }


    def get_object(self):
        if self.kwargs.get('pk') == 'me':
            return self.request.user
        return super().get_object()
