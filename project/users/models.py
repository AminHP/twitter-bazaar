from django.contrib.auth.models import AbstractUser

from ..commons.models import BaseModel


class User(AbstractUser, BaseModel):

    class Meta:
        ordering = ('-created_at', )
