from rest_framework import serializers

from .models import User


class UserBasicSerializer(serializers.ModelSerializer):

    old_password = serializers.CharField(required=False)

    class Meta:
        model = User

        fields = (
            'id', 'username', 'password', 'old_password', 'created_at',
        )

        extra_kwargs = {
            'password': {'write_only': True},
            'old_password': {'write_only': True},
            'created_at': {'read_only': True},
        }


    def create(self, validated_data):
        validated_data.pop('old_password', None)

        instance = super().create(validated_data)
        instance.set_password(instance.password)
        instance.save()

        return instance


    def update(self, instance, validated_data):
        if 'password' in validated_data:
            old_password = validated_data.pop('old_password', None)
            if not instance.check_password(old_password):
                raise serializers.ValidationError("Wrong 'old_passowrd'")

        instance = super().update(instance, validated_data)

        if 'password' in validated_data:
            instance.set_password(instance.password)
            instance.save()

        return instance


class UserFullSerializer(UserBasicSerializer):

    class Meta(UserBasicSerializer.Meta):
        fields = UserBasicSerializer.Meta.fields + (
            'modified_at',
        )

        extra_kwargs = {
            ** UserBasicSerializer.Meta.extra_kwargs,
            ** {
                'modified_at': {'read_only': True},
            }
        }


class UserInnerSerializer(serializers.ModelSerializer):

    class Meta:
        model = User

        fields = (
            'id', 'username',
        )
