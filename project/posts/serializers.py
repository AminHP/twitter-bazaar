from rest_framework import serializers

from .models import Post
from ..comments.models import Comment
from ..users.serializers import UserInnerSerializer


class CommentInnerBasicSerializer(serializers.ModelSerializer):

    user = UserInnerSerializer(read_only=True)

    class Meta:
        model = Comment
        fields = ('id', 'user', 'text', 'created_at')


class CommentInnerFullSerializer(CommentInnerBasicSerializer):

    class Meta(CommentInnerBasicSerializer.Meta):
        model = Comment

        fields = CommentInnerBasicSerializer.Meta.fields + (
            'modified_at',
        )



class PostBasicSerializer(serializers.ModelSerializer):

    user = UserInnerSerializer(read_only=True)

    class Meta:
        model = Post

        fields = ('id', 'user', 'text', 'created_at')

        extra_kwargs = {
            'created_at': {'read_only': True},
        }


class PostDetailedBasicSerializer(PostBasicSerializer):

    comments = CommentInnerBasicSerializer(read_only=True, many=True)

    class Meta(PostBasicSerializer.Meta):

        fields = PostBasicSerializer.Meta.fields + ('comments', )



class PostFullSerializer(PostBasicSerializer):

    class Meta(PostBasicSerializer.Meta):
        fields = PostBasicSerializer.Meta.fields + (
            'modified_at',
        )

        extra_kwargs = {
            ** PostBasicSerializer.Meta.extra_kwargs,
            ** {
                'modified_at': {'read_only': True},
            }
        }


class PostDetailedFullSerializer(PostFullSerializer):

    comments = CommentInnerFullSerializer(read_only=True, many=True)

    class Meta(PostFullSerializer.Meta):

        fields = PostFullSerializer.Meta.fields + ('comments', )



PostInnerSerializer = PostBasicSerializer
