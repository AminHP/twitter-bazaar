from django.contrib import admin

from .models import Post


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):

    list_display = (
        'id', 'user', 'text', 'created_at', 'modified_at',
    )

    search_fields = (
        'id', 'user__username', 'text',
    )
