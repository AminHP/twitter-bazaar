from rest_framework import viewsets
from rest_framework.decorators import action

from drf_psq import PsqMixin, Rule, psq

from .models import Post
from .serializers import (
    PostBasicSerializer, PostDetailedBasicSerializer,
    PostFullSerializer, PostDetailedFullSerializer,
)
from ..commons.permissions import (
    AllowAny, IsAuthenticated, IsAdminUser, AllowOptions,
)
from .permissions import IsSelf


class PostViewSet(PsqMixin, viewsets.ModelViewSet):

    queryset = Post.objects.all()
    serializer_class = PostFullSerializer
    permission_classes = [AllowAny | AllowOptions]

    filterset_fields = {
        'user__id': ['exact'],
    }

    psq_rules = {
        ('list', 'create', 'retrieve'): [
            Rule([IsAdminUser], PostFullSerializer),
            Rule([IsAuthenticated], PostBasicSerializer)
        ],

        ('update', 'partial_update', 'destroy'): [
            Rule([IsAdminUser], PostFullSerializer),
            Rule([IsAuthenticated & IsSelf], PostBasicSerializer),
        ],
    }


    def perform_create(self, serializer):
        serializer.validated_data['user'] = self.request.user
        return super().perform_create(serializer)


    @action(detail=False, methods=['GET'], url_path='detailed')
    @psq([
        Rule(
            [IsAdminUser], PostDetailedFullSerializer,
            queryset = lambda self: self.get_queryset_list_detailed()
        ),
        Rule(
            [IsAuthenticated], PostDetailedBasicSerializer,
            queryset = lambda self: self.get_queryset_list_detailed()
        )
    ])
    def list_detailed(self, request, *args, **kwargs):
        return super().list(self, request, *args, **kwargs)

    def get_queryset_list_detailed(self):
        return self.queryset.order_by('-modified_at')
