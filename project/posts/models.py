from django.db import models

from ..commons.models import BaseModel
from ..users.models import User


class Post(BaseModel):

    text = models.TextField(null=False, blank=True)

    user = models.ForeignKey(
        User, related_name='posts', on_delete=models.SET_DEFAULT, default=0
    )

    class Meta:
        ordering = ('-created_at', )


    def __str__(self):
        return str(self.pk)
