from rest_framework import permissions as drf_permissions

from ..jwt_tokens.permissions import IsSimpleOrJwtPermitted


AllowAny = drf_permissions.AllowAny

IsAuthenticated = drf_permissions.IsAuthenticated & IsSimpleOrJwtPermitted

IsAdminUser = drf_permissions.IsAdminUser & IsSimpleOrJwtPermitted


class AllowOptions(drf_permissions.BasePermission):

    def has_permission(self, request, view):
        return request.method == 'OPTIONS'
