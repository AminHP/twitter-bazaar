from rest_framework_simplejwt import views as jwt_views

from .serializers import TokenObtainPairSerializer


class TokenObtainPairView(jwt_views.TokenObtainPairView):

    serializer_class = TokenObtainPairSerializer
