from django.conf import settings

from rest_framework import permissions

from rest_framework_simplejwt.tokens import Token as JwtToken


class IsSimpleOrJwtPermitted(permissions.BasePermission):

    def has_permission(self, request, view):
        token = request.auth
        if not isinstance(token, JwtToken):
            return True

        action_name = view.__name__
        base_name = request._request.resolver_match.url_name.split('-')[0]
        action = f"{base_name}-{action_name}"

        allowed_actions = getattr(settings, 'JWT_ALLOWED_ACTIONS', [])
        if not action in allowed_actions:
            return False

        value = request.auth['action_permissions'].get(action)
        return bool(value == True)
