from django.urls import path

from rest_framework_simplejwt.views import (
    TokenRefreshView, TokenVerifyView
)

from .views import TokenObtainPairView


urlpatterns = [
    path(
        'api/jwt_tokens/', TokenObtainPairView.as_view(),
        name = 'jwt_token_obtain_pair'
    ),

    path(
        'api/jwt_tokens/refresh/', TokenRefreshView.as_view(),
        name = 'jwt_token_refresh'
    ),

    path(
        'api/jwt_tokens/verify/', TokenVerifyView.as_view(),
        name = 'jwt)token_verify'
    ),
]
