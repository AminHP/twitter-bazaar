from django.apps import AppConfig


class JwtTokensConfig(AppConfig):
    name = 'jwt_tokens'
