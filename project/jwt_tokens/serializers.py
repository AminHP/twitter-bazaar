from rest_framework import serializers

from rest_framework_simplejwt import serializers as jwt_serializers


class TokenObtainPairSerializer(jwt_serializers.TokenObtainPairSerializer):

    action_permissions = serializers.DictField()

        
    def validate(self, attrs):
        data = super().validate(attrs)

        refresh = self.get_token(self.user)

        refresh['action_permissions'] = self.initial_data['action_permissions']

        data['refresh'] = str(refresh)
        data['access'] = str(refresh.access_token)

        return data
