from django.db import models

from ..commons.models import BaseModel
from ..users.models import User
from ..posts.models import Post


class Comment(BaseModel):

    text = models.TextField(null=False, blank=True)

    user = models.ForeignKey(
        User, related_name='comments', on_delete=models.SET_DEFAULT, default=0
    )

    post = models.ForeignKey(
        Post, related_name='comments', on_delete=models.SET_DEFAULT, default=0
    )

    class Meta:
        ordering = ('-created_at', )


    def __str__(self):
        return str(self.pk)
