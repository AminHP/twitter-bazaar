from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page

from rest_framework import viewsets

from drf_psq import PsqMixin, Rule

from .models import Comment
from .serializers import (
    CommentBasicSerializer, CommentEditBasicSerializer,
    CommentFullSerializer, CommentEditFullSerializer,
)
from ..commons.permissions import (
    AllowAny, IsAuthenticated, IsAdminUser, AllowOptions,
)
from .permissions import IsSelf


class CommentViewSet(PsqMixin, viewsets.ModelViewSet):

    queryset = Comment.objects.all()
    serializer_class = CommentFullSerializer
    permission_classes = [AllowAny | AllowOptions]

    filterset_fields = {
        'post__id': ['exact'],
    }

    psq_rules = {
        ('list', 'create', 'retrieve'): [
            Rule([IsAdminUser], CommentFullSerializer),
            Rule([IsAuthenticated], CommentBasicSerializer)
        ],

        ('update', 'partial_update'): [
            Rule([IsAdminUser], CommentEditFullSerializer),
            Rule([IsAuthenticated & IsSelf], CommentEditBasicSerializer),
        ],

        'destroy': [
            Rule([IsAdminUser], CommentFullSerializer),
            Rule([IsAuthenticated & IsSelf], CommentBasicSerializer),
        ],
    }


    def perform_create(self, serializer):
        serializer.validated_data['user'] = self.request.user
        return super().perform_create(serializer)


    @method_decorator(cache_page(5 * 60))
    def retrieve(self, request, *args, **kwargs):
        return super().retrieve(self, request, *args, **kwargs)
