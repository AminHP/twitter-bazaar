from django.conf.urls import include, url
from rest_framework.routers import DefaultRouter

from . import views


router = DefaultRouter()
router.register(r'comments', views.CommentViewSet, basename='comment')


urlpatterns = [
    url(r'^api/', include((router.urls, 'comments')))
]
