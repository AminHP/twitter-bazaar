from django.contrib import admin

from .models import Comment


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):

    list_display = (
        'id', 'user', 'post', 'text', 'created_at', 'modified_at',
    )

    search_fields = (
        'id', 'user__username', 'post__pk', 'text',
    )
