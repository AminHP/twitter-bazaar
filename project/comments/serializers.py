from rest_framework import serializers

from .models import Comment
from ..posts.models import Post
from ..users.serializers import UserInnerSerializer
from ..posts.serializers import PostInnerSerializer


class CommentBasicSerializer(serializers.ModelSerializer):

    user = UserInnerSerializer(read_only=True)

    post = serializers.PrimaryKeyRelatedField(
        allow_null = False, queryset = Post.objects.all()
    )

    class Meta:
        model = Comment

        fields = ('id', 'user', 'post', 'text', 'created_at')

        extra_kwargs = {
            'created_at': {'read_only': True},
        }


    def to_representation(self, instance):
        ret = super().to_representation(instance)
        post = PostInnerSerializer(instance.post).data
        ret['post'] = post
        return ret


class CommentEditBasicSerializer(CommentBasicSerializer):

    post = PostInnerSerializer(read_only=True)



class CommentFullSerializer(CommentBasicSerializer):

    class Meta(CommentBasicSerializer.Meta):
        fields = CommentBasicSerializer.Meta.fields + (
            'modified_at',
        )

        extra_kwargs = {
            ** CommentBasicSerializer.Meta.extra_kwargs,
            ** {
                'modified_at': {'read_only': True},
            }
        }


class CommentEditFullSerializer(CommentFullSerializer):

    post = PostInnerSerializer(read_only=True)


CommentInnerSerializer = CommentBasicSerializer
