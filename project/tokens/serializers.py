from rest_framework import serializers

from .models import Token
from ..users.serializers import UserInnerSerializer


class TokenSerializer(serializers.ModelSerializer):

    user = UserInnerSerializer(read_only=True)

    class Meta:
        model = Token
        fields = '__all__'


class LoginSerializer(serializers.Serializer):

    username = serializers.CharField()
    password = serializers.CharField()
