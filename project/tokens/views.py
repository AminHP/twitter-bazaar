from django.contrib.auth import authenticate

from rest_framework import viewsets
from rest_framework import mixins
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.response import Response

from drf_psq import PsqMixin, Rule

from .models import Token
from .serializers import TokenSerializer, LoginSerializer
from ..commons.permissions import (
    AllowAny, IsAuthenticated, IsAdminUser, AllowOptions,
)


class TokenViewSet(PsqMixin,
                   mixins.ListModelMixin,
                   mixins.RetrieveModelMixin,
                   mixins.DestroyModelMixin,
                   viewsets.GenericViewSet):

    queryset = Token.objects.all()
    serializer_class = TokenSerializer
    permission_classes = [AllowAny | AllowOptions]

    psq_rules = {
        ('list', 'retrieve', 'destroy'): [Rule([IsAdminUser])],

        'logout': [Rule([IsAuthenticated])],
    }


    @action(detail=False, methods=['POST'])
    def login(self, request, *args, **kwargs):
        serializer = LoginSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        user = authenticate(**serializer.validated_data)
        if user is None:
            return Response(status=status.HTTP_404_NOT_FOUND)

        token = Token.objects.get_or_create(user=user)[0]
        result = dict(token=token.key, user_id=user.id)

        return Response(result, status=status.HTTP_200_OK)


    @action(detail=False, methods=['POST'])
    def logout(self, request, *args, **kwargs):
        request.user.auth_token.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
