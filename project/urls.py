from django.urls import path
from django.conf.urls import url, include
from django.contrib import admin

from rest_framework import permissions
from drf_yasg2.views import get_schema_view
from drf_yasg2 import openapi


schema_view = get_schema_view(
    openapi.Info(
        title = "Project API",
        default_version = 'v0',
        description = "Project API Documentation",
    ),
    public = True,
    permission_classes = (permissions.AllowAny, ),
)


urlpatterns = [
    path('admin/', admin.site.urls),

    url(r'^docs/swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    url(r'^docs/swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    url(r'^docs/redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),

    url(r'^', include('project.tokens.urls')),
    url(r'^', include('project.jwt_tokens.urls')),
    url(r'^', include('project.users.urls')),
    url(r'^', include('project.posts.urls')),
    url(r'^', include('project.comments.urls')),
]
